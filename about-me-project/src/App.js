import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="bg-gray-200 min-h-screen flex items-center justify-center">
      <div className="max-w-md w-full bg-white rounded-lg shadow-md overflow-hidden">
        <div className="p-8">
          <div className="text-center">
            <h1 className="text-2xl font-bold mb-2 text-blue-500">Aji Rama Wangsa</h1>
            <p className="text-gray-600 text-sm mb-4">NIM: 043618281</p>
          </div>
          <div className="border-t border-gray-300 pt-4">
            <p><span className="font-bold">Jenis Kelamin:</span> Laki-laki</p>
            <p><span className="font-bold">Program Studi:</span> Sistem Informasi</p>
            <p><span className="font-bold">Fakultas:</span> FST</p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
